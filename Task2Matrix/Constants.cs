﻿using System;
namespace Task2Matrix
{
    public class Constants
    {
        public const string SizeError = "Данные введены некорректно,введены недопустимые символы!Cтроками и столбцами матрицы могут быть только целые числа\n";
        public const string InputRequest = "Введите строки/столбцы матрицы: \n";
        public const string TraceTitle = "\nСлед матрицы:";
        public const string SpiralMatrixTitle = "\nСпиральный вывод:";
    }
}
