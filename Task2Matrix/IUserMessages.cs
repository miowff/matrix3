﻿namespace Task2Matrix
{
    public interface IUserMessages
    {
        public string InputValue();
        public string PrintMessage(string message);
    }
}
