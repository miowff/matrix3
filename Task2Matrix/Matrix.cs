﻿using System;
using System.Collections.Generic;
namespace Task2Matrix
{
    public class Matrix
    {
        public int columns { get; private set; }
        public int rows { get; private set; }
        int[,] matrix;
        public Matrix(int rows, int columns)
        {
            if (rows == 0 || columns == 0)
            {
                throw new InvalidOperationException("Некорректный ввод");
            }
            this.rows = rows;
            this.columns = columns;
            this.matrix = new int[rows, columns];
            FillMatrix();
        }
        public Matrix(int[,] matrix)
        {
            if (matrix == null)
            {
                throw new InvalidOperationException("Некорректный ввод");
            }
            this.rows = matrix.GetLength(0);
            this.columns = matrix.GetLength(1);
            this.matrix = matrix;
        }

     
        public int GetMatrixTrace()
        {
            int matrixTrace = 0;
            int min = this.rows < columns ? this.rows : this.columns;
            for (int i = 0; i < min; i++)
            {
                matrixTrace += matrix[i, i];
            }
            return matrixTrace;
        }
        public int[,] ReturnMatrix()
        {
            return matrix;
        }
        public int[] SpiralArray()
        {
            int startColumn = 0;
            int startRow = 0;
            int endColumn = matrix.GetLength(1) - 1;
            int endRow = matrix.GetLength(0) - 1;
            int[] matrixElements = new int[columns * rows];
            int k = 0;
            while (startColumn <= endColumn && startRow <= endRow)
            {
                this.AddColumnIntoArray(matrixElements, ref k, startColumn, endColumn, startRow);
                startRow++;
                this.AddRowIntoArray(matrixElements, ref k, startRow, endRow, endColumn);
                endColumn--;
                this.AddColumnIntoArray(matrixElements, ref k, startColumn, endColumn, endRow, true);
                endRow--;
                this.AddRowIntoArray(matrixElements, ref k, startRow, endRow, startColumn, true);
                startColumn++;
            }
            return matrixElements;
        }

        private void AddRowIntoArray(int[] _baseArray, ref int _arrayStartPoint, int _startRow, int _endRow, int _column, bool _isReverse = false)
        {
            int j = _isReverse ? _endRow : _startRow;
            for (; !_isReverse && j <= _endRow || _isReverse && j >= _startRow; j = _isReverse ? --j : ++j)
            {
                _baseArray[_arrayStartPoint] = matrix[j, _column];
                _arrayStartPoint++;
            }
        }
        private void AddColumnIntoArray(int[] _baseArray, ref int _arrayStartPoint, int _startColumn, int _endColumn, int _row, bool _isReverse = false)
        {
            int j = _isReverse ? _endColumn : _startColumn;
            for (; !_isReverse && j <= _endColumn || _isReverse && j >= _startColumn; j = _isReverse ? --j : ++j)
            {
                _baseArray[_arrayStartPoint] = matrix[_row, j];
                _arrayStartPoint++;
            }
        }
        private void FillMatrix()
        {
            Random _randomNumber = new Random();
            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    this.matrix[i, j] = _randomNumber.Next(0, 101);
                }
            }

        }
    }
}
