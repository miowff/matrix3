﻿using System;
namespace Task2Matrix
{
    public class ConsoleCommunication : IUserMessages
    {
        private string _message { get; set; }
        public string InputValue()
        {
            this._message = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(this._message))
            {
                PrintMessage(Constants.SizeError);
                this._message = Console.ReadLine();
            }
            return this._message;
        }

        public string PrintMessage(string message)
        {
            this._message = message;
            Console.Write(message);
            return message;
        }
    }
}
