﻿
namespace Task2Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.OutputEncoding = System.Text.Encoding.UTF8;
            ConsoleCommunication consoleCommunication = new ConsoleCommunication();
            ProgramManager programManager = new ProgramManager(consoleCommunication);
            programManager.Start();
        }
    }
}
