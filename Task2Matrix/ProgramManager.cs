﻿using System;
namespace Task2Matrix
{
    public class ProgramManager
    {
        private readonly IUserMessages _consoleCommunication = new ConsoleCommunication();
        public ProgramManager(IUserMessages consoleCommunication)
        {
            this._consoleCommunication = consoleCommunication ?? throw new ArgumentNullException(nameof(consoleCommunication));
        }
        public void PrintMatrix(int[,] _matrix)
        {
            if(_matrix==null)
            {
                throw new ArgumentNullException(nameof(_matrix));

            }
            for (int i = 0; i < _matrix.GetLength(0); i++)
            {
                for (int j = 0; j < _matrix.GetLength(1); j++)
                {
                    Console.ForegroundColor = i != j ? ConsoleColor.White : ConsoleColor.Red;
                    _consoleCommunication.PrintMessage(String.Format("{0,3}", _matrix[i, j]));
                }
                _consoleCommunication.PrintMessage("\n");
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
        private bool IsMatrixSizeCorrect(string _inputMessage)
        {
            if (string.IsNullOrEmpty(_inputMessage))
            {
                throw new ArgumentNullException(nameof(_inputMessage));
            }
            uint _valueFlag;
            if (uint.TryParse(_inputMessage, out _valueFlag))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string GetUserInput()
        {
            string message;
            _consoleCommunication.PrintMessage(Constants.InputRequest);
            message = _consoleCommunication.InputValue();
            while (IsMatrixSizeCorrect(message) == false)
            {
                _consoleCommunication.PrintMessage(Constants.SizeError);
                message = _consoleCommunication.InputValue();
            }
            return message;
        }
        public void Start()
        {
            int rows = int.Parse(GetUserInput());
            int columns = int.Parse(GetUserInput());
            Matrix matrix = new Matrix(rows, columns);
            PrintMatrix(matrix.ReturnMatrix());
            _consoleCommunication.PrintMessage(Constants.TraceTitle + matrix.GetMatrixTrace());
            _consoleCommunication.PrintMessage(Constants.SpiralMatrixTitle + string.Join(",", matrix.SpiralArray()));
        }

    }
}

