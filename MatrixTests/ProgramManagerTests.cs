﻿using NUnit.Framework;
using Task2Matrix;
using Moq;
namespace MatrixTests
{
    public class ProgramManagerTests
    {
        private readonly Mock<IUserMessages> _userMessagesMock = new Mock<IUserMessages>();
        private ProgramManager _programManager;
        [SetUp]
        public void Setup()
        {
            _programManager = new ProgramManager(_userMessagesMock.Object);
        }
        [Test]
        public void PrintMatrixTest()
        {
            //Arrange
            int[,] staticMatrix = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            Matrix matrix = new Matrix(staticMatrix);
            _userMessagesMock.Setup(x => x.PrintMessage(It.IsAny<string>()));
            //Act
            _programManager.PrintMatrix(matrix.ReturnMatrix());
            //Assert
            _userMessagesMock.Verify(x => x.PrintMessage(It.IsAny<string>()), Times.AtLeast(9));
        }

        [Test]
        public void StartTest()
        {
            //Arrange
            _userMessagesMock.Setup(x => x.InputValue()).Returns("3");

            _userMessagesMock.Setup(x => x.PrintMessage(It.IsAny<string>()));
            //Act
            _programManager.Start();
            //Assert
            _userMessagesMock.Verify(x => x.PrintMessage(It.IsAny<string>()), Times.AtLeast(2));
        }
    }
}