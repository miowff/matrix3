using NUnit.Framework;
using Task2Matrix;
namespace MatrixTests
{
    public class MatrixTests
    {
        [Test]

        public void GetMatrixTraceTest()
        {
            //arrange
            int[,] staticMatrix = new int[,] { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 }, { 16, 17, 18, 19, 20 } };
            int expectedTrace = 40;

            //act
            Matrix matrix = new Matrix(staticMatrix);
            //assert
            Assert.AreEqual(expectedTrace, matrix.GetMatrixTrace());
        }
        [Test]
        public void SpiralPrintTest()
        {
            //arrange
            int[,] staticMatrix = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            int[] expectedSpiralArray = new int[] { 1, 2, 3, 6, 9, 8, 7, 4, 5 };
            //act
            Matrix matrix = new Matrix(staticMatrix);
            //assert
            Assert.AreEqual(expectedSpiralArray[8], matrix.SpiralArray()[8]);
        }
        [Test]
        public void CreationTest()
        {
            //arrange
            int[,] staticMatrix = new int[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
            //act
            Matrix matrix = new Matrix(staticMatrix);
            //assert
            Assert.AreEqual(staticMatrix.Rank, matrix.ReturnMatrix().Rank);
        }
        [Test]
        public void IsMatrixNullTest()
        {
            //arrange
            Matrix matrix = new Matrix(3, 3);
            //assert
            Assert.IsNotNull(matrix);
        }
        [Test]
        public void IsMatrixContainsZeroOrHundred()
        {
            //arrange
            Matrix matrix = new Matrix(2000, 2000);
            int isHunderdOrNull=1;
            //act
            for(int i=0;i<2000;i++)
            {
                for(int j=0;j<2000; j++)
                {
                    if (matrix.ReturnMatrix()[i, j] == 0 ||matrix.ReturnMatrix()[i, j] == 100)
                    { 
                        isHunderdOrNull = matrix.ReturnMatrix()[i, j]; 
                    }
                }
            }
            //assert
            Assert.AreEqual(0,100,isHunderdOrNull);
        }
    }
}