﻿using NUnit.Framework;
using Task2Matrix;
using System.IO;
using System;
namespace MatrixTests
{
    public class ConsoleCommiunicationTests
    {
        private ConsoleCommunication _consoleCommunication;
        [SetUp]
        public void Setup()
        {
            _consoleCommunication = new ConsoleCommunication();
        }
        [Test]
        public void UserInputTest()
        {
            //Arrange
            string expected = "3";
            var sr = new StringReader(expected);
            //Act
            Console.SetIn(sr);
            var actual = _consoleCommunication.InputValue();
            //Assert
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void PrintMessageTest()
        {
            //Arrange
            string expected = "3";
            //Act
            string actual= _consoleCommunication.PrintMessage(expected);
            //Assert
            Assert.AreEqual(actual, expected);
        }
    }
}